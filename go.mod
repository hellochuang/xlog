module gitee.com/hellochuang/xlog

go 1.18

require (
	github.com/golang/protobuf v1.5.3
	github.com/golang/snappy v0.0.4
	github.com/prometheus/common v0.45.0
	github.com/sirupsen/logrus v1.9.3
	google.golang.org/protobuf v1.31.0
)

require golang.org/x/sys v0.13.0 // indirect
