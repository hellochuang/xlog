# 说明

该日志库基于logrus的顶层封装。支持设置日志级别，设置日志保存到文档或者输出到loki

# 用法

1. 安装 go get gitee.com/hellochuang/xlog@v1.0.0

2. 示例

3. 简单用法
```go
package ttt
import (
    "fmt"
    "gitee.com/hellochuang/xlog"
    "github.com/lestrrat-go/file-rotatelogs"
    "github.com/rifflock/lfshook"
    "github.com/sirupsen/logrus"
    "runtime"
    "testing"
    "time"
)

func Test_all(t *testing.T) {
    // 设置日志级别
    xlog.SetLevel("trace")
    // 开启堆栈信息
    xlog.SetCaller(true)
    // 针对不同的服务可以自己设置不同的service name
    xlog.Name("uas")
    //输出到loki，本质上是内置了一个loki的hook
    xlog.SetLoki("http://127.0.0.1:3000")
    xlog.Info("i love you golang")

// 自定义hook,所谓hook就是在输出前拦截对应日志信息，自行处理，此处以开源的github.com/lestrrat-go/file-rotatelogs和github.com/rifflock/lfshook作为demo
// 具体用法不在此包叙述中
    writer, err := rotatelogs.New(
        "logDir/log.%Y-%m-%d",
        rotatelogs.WithRotationTime(time.Hour*24),
        rotatelogs.WithRotationCount(10),
    )
    if err != nil {
        panic(err)
    }
    h := lfshook.NewHook(lfshook.WriterMap{
            xlog.PanicLevel: writer,
            xlog.TraceLevel: writer,
            xlog.InfoLevel:  writer,
            xlog.DebugLevel: writer,
            xlog.FatalLevel: writer,
            xlog.WarnLevel:  writer,
            xlog.ErrorLevel: writer,
        }, &logrus.JSONFormatter{CallerPrettyfier: func(frame *runtime.Frame) (function string, file string) {
                return fmt.Sprintf("%s:%d", frame.File, frame.Line), ""
        }})
    xlog.GetLogger().AddHook(h)
    xlog.Pretty(t)
    xlog.Info("自定义hook")
}

```

***so,go get and try it,it is so cool!!!***
